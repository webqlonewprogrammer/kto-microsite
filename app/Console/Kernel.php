<?php

namespace App\Console;

use DB;
use Mail;
use Carbon\Carbon;
use App\Mail\Reminder;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        // Send email reminder 1 day before RSVP date
        $schedule->call(function() {
            $datetimes = [
                '2018-06-27 00:00:00',
                '2018-06-28 00:00:00',
                '2018-06-29 00:00:00',
                '2018-06-30 00:00:00',
            ];

            if ( in_array( Carbon::today('Asia/Kuala_Lumpur'), $datetimes ) ) {
                $tomorrowDate = Carbon::tomorrow('Asia/Kuala_Lumpur')->format('Y-m-d');

                $reminderEmailList = DB::table('registrations')
                    ->where('reminder_sent', false)
                    ->whereDate('rsvp_date', $tomorrowDate)
                    ->limit(10)
                    ->get();

                foreach ($reminderEmailList as $registration) {
                    Mail::to( $registration->email )->send( new Reminder($registration->unique_code) );
                }
            }
        })->everyTenMinutes();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
