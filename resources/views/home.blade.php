@extends('layouts.app')

@section('content')
<div class="container-fluid top">
    <div class="background-image">
    </div>
    
    <div class="row">
        <div class="col-lg-6 wrapper">
            <img src="/images/nars_logo-min.png" alt="" class="logo-img">
            <div class="mobile-padding">
                <h1 class="spiked">SPIKED WITH COLOR.</h1>
                <h1 class="studded">STUDDED WITH STYLE.</h1>
                <h3 class="subtitle">All attitude. No limits.</h3>

                <h3 class="slogan">Sign up now to amp up your look for the<br>holidays and to receive a deluxe-size gift.*</h3>

                <div>
                    @yield('form')
                </div>
            </div>
        </div>
    </div>

    <div class="tnc text-center">
        
        <p>*Limited quantities available. Items may vary at different stores. <span class="mobile-tnc-br">NARS reserves the right to modify or amend the validity of this offer without prior notice. <span class="desktop-tnc-br">Other terms and conditions apply. NARS Cosmetics is available at Pavilion KL, Suria KLCC, Mid Valley Megamall, <span class="ipad-tnc-br">Sunway Pyramid, <span class="mobile-tnc-br-2">AEON Tebrau City, SkyAvenue Genting and Parkson Gurney.</span></span></span></span></p>
        
    </div>
</div>


@endsection

@push('js')
<script>
    $(function () {
        $('#video').on('click', function() {
            if($(this)[0].paused) {
                $(this)[0].play();
            }
            else {
                $(this)[0].pause();
            }
        });
    });
</script>
@endpush
