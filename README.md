KTO-microsite
1.) Copy and Modify .env
```
cp .env.example .env
```
2.) Run Composer and Node install to include all library
```
composer install
```

```
npm install(if neccesary)
```

3.)Generate Key

```
php artisan key:generate
```

4.)Migrate Database
```
php artisan migrate